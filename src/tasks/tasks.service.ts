import { TaskRepository } from './task.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import {TaskStatus} from './task.status.enum';
import {v4 as uuid} from 'uuid'
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository:TaskRepository
  ){}
 // private tasks: Task[] = [];
  // getAllTasks(): Task[] {
  //   return this.tasks;
  // }

  // getTasksWithFilter(filterDto:GetTasksFilterDto):Task[]{
  //   const {status, search} = filterDto;
  //   // console.log("filterDto",filterDto);
  //   let tasks = this.getAllTasks();

  //   if(status){
  //     tasks = tasks.filter((task)=> status===task.status
  //     );
  //   }

  //   if(search){
  //     tasks = tasks.filter((task)=>{
  //       if(task.title.includes(search) || task.description.includes(search)){
  //         return true;
  //       }
  //       return false;
  //     })
  //   }

  //   return tasks;
  // }
  getTasks(filterDto: GetTasksFilterDto):Promise<Task[]>{
    return this.taskRepository.getTasks(filterDto);
  }

  async getTaskById(id:string): Promise<Task>{
    const found = await this.taskRepository.findOne(id);

    if(!found){
      throw new NotFoundException(`task with Id "${id}" not found`);
    }
    return found;
  }


  // getTaskById(id:string): Task{
  //   const found = this.tasks.find((task) => task.id===id);

  //   if(!found){
  //     throw new NotFoundException(`task with Id "${id}" not found`);
  //   }
  //   return found;
  // }


  createTask(createTaskDto): Promise<Task>{
    return this.taskRepository.createTask(createTaskDto);
  }
  // createTask(createTaskDto): Task{
  //   const {title,description} = createTaskDto;
  //   const task: Task = {
  //     id: uuid(),
  //     title,
  //     description,
  //     status: TaskStatus.OPEN,
  //   }
  //   this.tasks.push(task);
  //   return task;
  // }

  async deleteTask(id:string): Promise<void>{
    const result = await this.taskRepository.delete(id);
    if(result.affected===0){
      throw new NotFoundException(`task with Id "${id}" not found`);
    }
  }
  // deleteTask(id:string): void{
  //   const found = this.getTaskById(id);
  //   this.tasks = this.tasks.filter((task) => task.id!=found.id);
  // }

  async updateTaskStatus(id:string, status: TaskStatus): Promise<Task>{
    const task = await this.getTaskById(id);
    task.status = status;
    await this.taskRepository.save(task);
    return task;
  }

  // updateTaskStatus(id:string, status: TaskStatus): Task{
  //   const task = this.getTaskById(id);
  //   task.status = status;
  //   return task;
  // }
}
